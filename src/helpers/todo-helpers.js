import uuid from 'uuid/v4';

export function makeTodo({ id = uuid(), value, completed = false, createdAt = Date.now() }) {
  return {
    id,
    value,
    completed,
    createdAt,
  };
}

const LOCAL_STORAGE_KEY = 'todos';

export function saveToLocalStorage(todos) {
  localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
}

export function getFromLocalStorage() {
  try {
    const todos = localStorage.getItem(LOCAL_STORAGE_KEY);
    if(todos) {
      return JSON.parse(todos);
    }
  } catch(err) {
    console.error('error getting from local storage', err);
  }

  return [];
}
