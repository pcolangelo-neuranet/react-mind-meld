import React, { useRef } from "react";

const noop = () => {};

const ENTER_KEY_CODE = 13;

function NewTodo({ onNewTodo = noop, onSelectAll = noop }) {
  const inputRef = useRef();

  function onKeyUp(event) {
    if(event.keyCode === ENTER_KEY_CODE && event.currentTarget.value) {
      onNewTodo(event.currentTarget.value);
      inputRef.current.value = '';
    }    
  }

  return (
    <div className="new-todo">
      <input ref={inputRef} type="text" className="new-todo__input" onKeyUp={onKeyUp} />
    </div>
  );
}

export default NewTodo;
