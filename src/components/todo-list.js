import React from 'react';
import TodoItem from './todo-item';

function TodoList({ todos = [], onItemChange, onItemDelete }) {
  const todoItems = todos.map((todo) => (
    <TodoItem todo={todo} onChange={onItemChange} onDelete={onItemDelete} />
  ));

  return (
    <div>
      {todoItems}
    </div>
  );
}

export default TodoList;
