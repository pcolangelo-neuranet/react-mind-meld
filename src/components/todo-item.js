import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

function TodoItem({ todo, onChange, onDelete }) {
  function onCompletedChanged(event) {
    onChange({
      ...todo,
      completed: event.target.checked
    });
  }

  function onDeleteClicked() {
    onDelete(todo.id);
  }

  return (
    <div className="todo-item">
      <div className="todo-item__left">
        <Checkbox color="primary" checked={todo.completed} onChange={onCompletedChanged} />
      </div>
      
      <div className="todo-item__middle">
        <div className={'todo-item__text ' + (todo.completed ? 'todo-item__text--completed' : '')}>
          {todo.value}
        </div>
      </div>

      <div className="todo-item__right">
        <IconButton onClick={onDeleteClicked}>
          <DeleteIcon />
        </IconButton>
      </div>
    </div>
  );
}

export default TodoItem;
