import React, { Component } from "react";
import Header from "./components/header";
import NewTodo from "./components/new-todo";
import { makeTodo, saveToLocalStorage, getFromLocalStorage } from "./helpers/todo-helpers";
import TodoList from "./components/todo-list";

class TodoApp extends Component {
  state = {
    todos: getFromLocalStorage()
  };

  onNewTodo = (value) => {
    const { todos } = this.state;
    const newTodo = makeTodo({ value });

    const newTodos = todos.concat(newTodo);
    this.setState({
      todos: newTodos
    });
    saveToLocalStorage(newTodos);
  }

  onTodoItemChanged = (updatedTodo) => {
    const { todos } = this.state;

    const newTodos = todos.map((todo) => {
      if(todo.id === updatedTodo.id) {
        return updatedTodo;
      }
      return todo;
    });
    this.setState({
      todos: newTodos
    });
    saveToLocalStorage(newTodos);
  }

  onTodoItemDelete = (id) => {
    const { todos } = this.state;

    const newTodos = todos.filter((todo) => todo.id !== id)
    this.setState({
      todos: newTodos
    });
    saveToLocalStorage(newTodos);
  }

  render() {
    const { todos } = this.state;

    return (
      <div>
        <Header />
        <NewTodo onNewTodo={this.onNewTodo} />
        <TodoList 
          todos={todos} 
          onItemChange={this.onTodoItemChanged} 
          onItemDelete={this.onTodoItemDelete} 
        />
      </div>
    );
  }

}

export default TodoApp;
