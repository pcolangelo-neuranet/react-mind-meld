import React from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './todo-app';

// Not how we would import in a production app!
import './styles/main.scss';

ReactDOM.render(<TodoApp />, document.getElementById('root'));
